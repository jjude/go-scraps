# Command line program

Our goal in this tutorial is to build a simple command-line based calculator. In this post, we will provide only one operator, add. If you input `add 4 4`, it will return 8.

Read the detailed [blog post](https://jjude.com/go-cmd) for more.
