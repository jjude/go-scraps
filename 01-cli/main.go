package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	cli "gopkg.in/urfave/cli.v1"
)

func main() {
	app := cli.NewApp()
	app.Name = "Calc"
	app.Usage = "A calculator tool"
	app.Description = "For support, contact ..email.."
	app.UsageText = "calc <op> <num1> <num2>"
	app.Version = "1.0.0"
	app.Authors = []cli.Author{
		cli.Author{
			Name: "Joseph Jude",
		},
	}

	app.Commands = []cli.Command{
		{
			Name:    "add",
			Aliases: []string{"a"},
			Usage:   "add two numbers",
			Action: func(c *cli.Context) error {
				a, _ := strconv.Atoi(c.Args().Get(0))
				b, _ := strconv.Atoi(c.Args().Get(1))
				fmt.Printf("%v\n", a+b)
				return nil
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}
