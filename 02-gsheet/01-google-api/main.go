package main

import (
	"context"
	"fmt"
	"io/ioutil"

	"golang.org/x/oauth2/google"
	"google.golang.org/api/sheets/v4"
)

func checkError(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func main() {
	/*
		ref: https://cloud.google.com/docs/authentication/getting-started
		goto: https://console.cloud.google.com/apis/credentials/serviceaccountkey
		From the Service account drop-down list, select New service account.
		Enter a name into the Service account name field.
		From the Role drop-down list, select Project > Owner.
		Click Create. A JSON file that contains your key downloads to your computer.
		Rename this file as secret.json and exclude from git (add to .gitignore)

		https://developers.google.com/sheets/api/quickstart/go has the original code
	*/
	data, err := ioutil.ReadFile("secret.json")
	checkError(err)
	conf, err := google.JWTConfigFromJSON(data, sheets.SpreadsheetsScope)
	checkError(err)

	client := conf.Client(context.TODO())
	srv, err := sheets.New(client)
	checkError(err)

	spreadsheetID := "1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms"
	readRange := "Class Data!A2:E"
	resp, err := srv.Spreadsheets.Values.Get(spreadsheetID, readRange).Do()
	checkError(err)

	if len(resp.Values) == 0 {
		fmt.Println("No data found.")
	} else {
		fmt.Println("Name, Major:")
		for _, row := range resp.Values {
			// Print columns A and E, which correspond to indices 0 and 4.
			fmt.Printf("%s, %s\n", row[0], row[4])
		}
	}

}
