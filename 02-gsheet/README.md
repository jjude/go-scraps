# Read Google Sheet with Golang

ref: https://cloud.google.com/docs/authentication/getting-started
goto: https://console.cloud.google.com/apis/credentials/serviceaccountkey
From the Service account drop-down list, select New service account.
Enter a name into the Service account name field.
From the Role drop-down list, select Project > Owner.
Click Create. A JSON file that contains your key downloads to your computer.
Rename this file as secret.json and exclude from git (add to .gitignore)
