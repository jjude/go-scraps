# Exercises to learn Golang

This repository contains small exercises to learn Golang.

## 1. Command line program

Write a cli calculator tool. Code is in [01-cli](01-cli/README.md) folder.

[Blog Post](https://jjude.com/go-cmd/)

# 2. Read google sheet

Read google sheet. Code is in [02-gsheet](02-gsheet/README.md) folder.

It is done using regular Google sheet api. As well as with an easier API `gopkg.in/Iwark/spreadsheet.v2`.

[Blog Post](https://jjude.com/gsheet-go)
